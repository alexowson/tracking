import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TrackingService} from "../services/tracking/tracking.service";

@Component({
    selector: 'app-folder',
    templateUrl: './folder.page.html',
    styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
    public folder: string;
    private isRunningService = false;
    private buttonText = "Start";

    constructor(
        private activatedRoute: ActivatedRoute,
        public trackingService: TrackingService
    ) {
    }

    ngOnInit() {
        this.folder = this.activatedRoute.snapshot.paramMap.get('id');
    }

    switchService() {
        if (!this.isRunningService) {
            this.isRunningService = true;
            this.trackingService.start();
            this.buttonText = "Stop";
        } else {
            this.isRunningService = false;
            this.trackingService.stop();
            this.buttonText = "Start";
        }
    }
}
