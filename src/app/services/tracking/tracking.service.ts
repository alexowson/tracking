import {Injectable} from '@angular/core';
import {DatePipe} from '@angular/common';
import {BackgroundMode} from '@ionic-native/background-mode/ngx';
import {Geolocation, GeolocationOptions} from '@ionic-native/geolocation/ngx';

export const PREFERENCE_KEY_DATE_LAST_TRACKING_SYNC = 'PREFERENCE_KEY_DATE_LAST_TRACKING_SYNC';

@Injectable({
    providedIn: 'root'
})
export class TrackingService {
    private refInterval;

    constructor(
        private backgroundMode: BackgroundMode,
        private geolocation: Geolocation,
    ) {
    }

    init() {
    }

    start() {
        console.log('Starting service...');
        this.backgroundMode.enable();
        this.backgroundMode.on('activate')
            .subscribe(value => {
                console.log('backgroundMode on:', 'activate');
                this.backgroundMode.disableWebViewOptimizations();
            });

        // const TRACK_TIME = 1000 * 60 * 5;
        const TRACK_TIME = 1000 * 10;
        this.refInterval = setInterval(()=>{
            const now = new Date();
            console.log("time=", now)

            this.getCurrentPosition().then((resp) => {
                this.savePosition(resp.coords.latitude, resp.coords.longitude).then();
            }).catch((error) => {
                console.log('Error getting location', error);
            });
        }, TRACK_TIME);
    }

    stop() {
        console.log('Stoping service...');
        this.backgroundMode.un('activate', () => {

        });
        this.backgroundMode.disable();
        clearInterval(this.refInterval);
    }

    private savePosition(latitude: number, longitude:number): Promise<any> {
        // console.log('TrackingService', 'savePosition');
        return new Promise((resolve, reject) => {
            // const now = new Date();
            // const pipe = new DatePipe('en-US');
            // const time =  pipe.transform(now.getTime(), 'yyyy-MM-ddTHH:mm:ss.SSS\'Z\'', 'UTC');
            // console.log("time=", time)
            console.log("latitude=", latitude, "longitude=", longitude)
            console.log("=======")
        });
    }

    private getCurrentPosition(): Promise<any> {
        return new Promise((resolve, reject) => {
            const options: GeolocationOptions = {
                // maximumAge: 5 * 60 * 1000,
                // timeout: 40 * 1000,
                enableHighAccuracy: true
            };
            this.geolocation.getCurrentPosition(options)
                .then((resp) => resolve(resp))
                .catch((error) => reject(error));
        });
    }
}
